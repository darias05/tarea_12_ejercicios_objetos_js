//Crear un metodo constructor llamado formulas. Construye los siguiente metodos para la clase:
//3.1 sumar(entero, entero) 3.2 fibonacci(cantidad) a partir de una entero sacar los numeros 
//3.3 operacion_modulo(cantidad) a partir de una cantidad mostrar cuales dan residuo 0 
//3.4 primos(cantidad) a partir de una cantidad mostrar cuales son numeros primos

function formulas(nom_persona,edad_persona,ID_persona){
    this.sumar=function(){
        let valor1=prompt("Ingrese el primer valor");
        let valor2=prompt("Ingrese el segundo valor valor");
        this.cantidad= parseInt(valor1) + parseInt(valor2);
        return `El primer valor es: ${valor1} / el segundo valor es: ${valor2} y la suma de ambos es: ${this.cantidad}`;
    }

    this.fibonacci= function(numero){
        fibo_total=0;
        for (let i=1; i<= numero; i++){
            fibo_total= fibo_total + i;
        }
        return fibo_total;
    }

    this.operacion_modulo= function(){
        let num= parseInt(prompt("Ingrese numero: "));

        for (let i=1;i<=num;i++){
            if (i%2==0){
                console.log("El numero: "+i+" da residuo 0")
            }
        }     
    }
    this.primos= function(){
        let num= parseInt(prompt("Ingrese numero: "));
        for (let i=1;i<=num;i++){
            if (i%2 != 0){
                console.log("El numero: "+i+" es primo")
            }
        }  
    }
}
    
