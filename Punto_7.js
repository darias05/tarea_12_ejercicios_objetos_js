//Agregar al contador del ejercicio 6, la capacidad de recordar un String que representa el último 
//comando que se le dio. Los Strings posibles son "reset", "incremento", "decremento" o "actualizacion" 
//(para el caso de que se invoque valorActual con un parámetro). Para saber el último comando, se le envía al 
//contador el mensaje ultimoComando().
//En el ejemplo del ejercicio 3, si luego de la secuencia indicada se evalúa contador.ultimoComando() 
//el resultado debe ser "incremento"

contador.prototype.ultCom= function(){
    return `Ultimo comando es: ${this.type}`
}