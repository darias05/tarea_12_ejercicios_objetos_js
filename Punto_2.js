//Crea un metodo constructor llamado cuenta que tendrá los siguientes atributos: titular 
//(que es nombre de la persona) y cantidad. El titular será obligatorio y 
//la cantidad es opcional. Construye los siguientes métodos para el metodo:
// 2.1mostrar(): Muestra los datos de la cuenta. 2.2 ingresar(cantidad): 
//se ingresa una cantidad a la cuenta, si la cantidad introducida es negativa,
// no se hará nada. 2.3 retirar(cantidad): se retira una cantidad a la cuenta. 
//La cuenta puede estar en números rojos.

function cuenta(){
    this.titular;
    this.cantidad;
    this.crear_cuenta= function(){
        let nombre= prompt("Ingresa el nombre del titular: ");
        let cantidad_inicial= parseInt( prompt("Ingresa la cantidad inicial de la cuenta: "));
        if(cantidad_inicial < 0){
            return "Error el saldo inicial debe ser mayor a 0";
        }else{
            this.titular=nombre;
            this.cantidad= cantidad_inicial;
        }

    }
    this.mostrar= function(){
        return `Su saldo actual es de: $${this.cantidad}`;
    }
    this.ingresar= function(){
        let valor= prompt("Ingresa el valor que se agregara a tu cuenta");
        this.cantidad= this.cantidad+ parseInt(valor);
        return `Se agrego a tu cuenta: $${valor} y ahora tiene un saldo de: $${this.cantidad}`;
    }
    this.retirar=function(){
        let valor= prompt("Ingresa el valor que se va a retirar");
        this.cantidad= this.cantidad - parseInt(valor);
        return `Se retiro de tu cuenta: $${valor} y ahora tienes un saldo de: $${this.cantidad}`;
    }
}