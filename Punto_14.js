// 14. Un taller de diseño de autos quiere estudiar un nuevo prototipo. Para eso, nos
// piden hacer un metodo constructor concentrado en las características del motor. El prototipo
// de motor tiene 5 cambios (de primera a quinta), y soporta hasta 5000 RPM.
// La velocidad del auto se calcula así: (rpm / 100) * (0.5 + (cambio / 2)). P.ej. en
// tercera a 2000 rpm, la velocidad es 20 * (0.5 + 1.5) = 40.
// También nos interesa controlar el consumo. Se parte de una base de 0.05 litros por
// kilómetro. A este valor se le aplican los siguientes ajustes:
// Si el motor está a más de 3000 rpm, entonces se multiplica por
// (rpm - 2500) / 500.
// P.ej., a 3500 rpm hay que multiplicar por 2, a 4000 rpm por 3, etc.

// Si el motor está en primera, entonces se multiplica por 3.
// Si el motor está en segunda, entonces se multiplica por 2.
// Los efectos por revoluciones y por cambio se acumulan. P.ej. si el motor está en
// primera y a 5000 rpm, entonces el consumo es 0.05 * 5 * 3 = 0.75 litros/km.
// El metodo constructor debe entender estos mensajes:
// arrancar(), se pone en primera con 500 rpm.
// subirCambio()
// bajarCambio()
// subirRPM(cuantos)
// bajarRPM(cuantos)
// velocidad()
// consumoActualPorKm()

function molde_prototipo(){
    this.velocidad = 0
    this.consumo = 0

    this.arrancar = function(){
      this.cambio = 1
      this.RPM = 0
    }

    this.subirCambio = function(){
      if(this.cambio < 5){
         this.cambio = this.cambio + 1
      } else {
        console.log(`No se pueden subir mas cambios, el cambio es= ${this.cambio}`)
      }
    }

    this.bajarCambio = function(){
      if(this.cambio > 1){
         this.cambio = this.cambio - 1
      } else {
        console.log(`No se pueden bajar mas cambios, el cambio es= ${this.cambio}`)
      }
    }

    this.subirRPM = function(valor_subida){
      if(this.RPM < 5000){
         this.RPM = this.RPM + valor_subida
      } else {
        console.log(`No se pueden bajar mas cambios, los RPM actuales son= ${this.RPM}`)
      }
    }

    this.bajarRPM = function(valor_bajada){
      if(this.RPM > 0){
         this.RPM = this.RPM - valor_bajada
      } else {
        console.log(`No se pueden bajar mas cambios, los RPM actuales son= ${this.RPM}`)
      }
    }

    this.velocidad = function(){
      this.velocidad = (this.RPM / 100) * (0.5 + (this.cambio / 2))
      console.log(`La velocidad es= ${this.velocidad}`)
    }

    this.consumoActualPorKm = function(){
      if(this.RPM <= 3000){
        this.consumo = 0.05
        console.log(`Consumo de ${this.consumo} litros por kilómetro`)
      }  else if(this.RPM > 3000 && this.RPM <= 3500){
         this.consumo = 0.05 * ((this.RPM - 2500) / 500) * 1
      } else if(this.RPM > 3500 && this.RPM <= 4000){
         this.consumo = 0.05 * ((this.RPM - 2500) / 500) * 2
      } else{
         this.consumo = 0.05 * ((this.RPM - 2500) / 500) * 3
      }
    }
}

let prototipo_uno = new molde_prototipo()
prototipo_uno.arrancar()
prototipo_uno.subirCambio()
prototipo_uno.bajarCambio()
prototipo_uno.subirRPM(2000)
prototipo_uno.bajarRPM(500)
prototipo_uno.velocidad()
prototipo_uno.consumoActualPorKm()