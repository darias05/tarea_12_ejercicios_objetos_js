// 12. Se está pensando en el diseño de un juego que incluye la nave espacial Enterprise.
// En el juego, esta nave tiene un nivel de potencia de 0 a 100, y un nivel de coraza
// de 0 a 20. 
// La Enterprise puede encontrarse con una pila atómica, en cuyo caso su potencia aumenta en 25.
// encontrarse con un escudo, en cuyo caso su nivel de coraza aumenta en 10.
// recibir un ataque, en este caso se especifican los puntos de fuerza del ataque recibido.
// La Enterprise  para  el ataque con la coraza, y si la coraza no
// alcanza, el resto se descuenta de la potencia. P.ej. si la Enterprise con 80
// de potencia y 12 de coraza recibe un ataque de 20 puntos de fuerza, puede
// parar solamente 12 con la coraza, los otros 8 se descuentan de la potencia. La
// nave debe quedar con 72 de potencia y 0 de coraza. Si la Enterprise no tiene
// nada de coraza al momento de recibir el ataque, entonces todos los puntos de
// fuerza del ataque se descuentan de la potencia.
// La potencia y la coraza tienen que mantenerse en los rangos indicados, p.ej. si la
// Enterprise tien 16 puntos de coraza y se encuentra con un escudo, entonces queda
// en 20 puntos de coraza, no en 26. Tampoco puede quedar negativa la potencia, a
// lo sumo queda en 0.
// La Enterprise nace con 50 de potencia y 5 de coraza.
// Implementar este metodo constructor de la Enterprise, que tiene que entender los siguientes
// mensajes:
// 12.1 potencia()
// 12.2 coraza()
// 12.3 encontrarPilaAtomica()
// 12.4 encontrarEscudo()
// 12.5 recibirAtaque(puntos)
// P.ej. sobre un REPL recién lanzado, después de esta secuencia
// enterprise.encontrarPilaAtomica()
// enterprise.recibirAtaque(14)
// enterprise.encontrarEscudo()
// la potencia de la Enterprise debe ser 66, y su coraza debe ser 10.

function molde_enterprise(){
    this.potencia_enterprise = 50
    this.coraza_enterprise = 5

    this.potencia = function(){
        console.log(`Potencia = ${this.potencia_enterprise}`)
    }

    this.coraza = function(){
        console.log(`Coraza = ${this.coraza_enterprise}`)
    }

    this.encontrarPilaAtomica = function(){
       if (this.potencia_enterprise > 0){
          if(this.potencia_enterprise + 25 > 100){
            this.potencia_enterprise = 100
          } else {
            this.potencia_enterprise = this.potencia_enterprise + 25
          }
       } else {
         console.log('La Enterprise ha muerto, no se le puede agregar mas potencia')
       }
    }

    this.encontrarEscudo = function(){
        if (this.potencia_enterprise > 0){
            if(this.coraza_enterprise + 10 > 20){
              this.coraza_enterprise = 20
            } else {
              this.coraza_enterprise = this.coraza_enterprise + 10
            }
        } else {
         console.log('La Enterprise ha muerto, no se le puede agregar mas escudo')
       }
    }

    this.recibirAtaque = function(puntos_ataque){
        this.valor_golpe = puntos_ataque
        if (this.potencia_enterprise > 0){
            if(this.coraza_enterprise - this.valor_golpe >= 0){
                this.coraza_enterprise = this.coraza_enterprise - this.valor_golpe
            } else if (this.coraza_enterprise - this.valor_golpe < 0){
                this.valor_golpe = this.valor_golpe - this.coraza_enterprise
                this.coraza_enterprise = 0
                if(this.potencia_enterprise - this.valor_golpe > 0){
                    this.potencia_enterprise = this.potencia_enterprise - this.valor_golpe
                } else {
                    this.coraza_enterprise = 0
                    this.potencia_enterprise = 0
                    console.log('La Enterprise ha muerto')
                    console.log(`Potencia = ${this.potencia_enterprise}`)
                    console.log(`Coraza = ${this.coraza_enterprise}`)
                }
            }
        } else {
            this.coraza_enterprise = 0
            this.potencia_enterprise = 0
            console.log('La Enterprise ha muerto')
            console.log(`Potencia = ${this.potencia_enterprise}`)
            console.log(`Coraza = ${this.coraza_enterprise}`)
        }
    }
}

let enterprise = new molde_enterprise()
enterprise.potencia()
enterprise.coraza()
enterprise.encontrarPilaAtomica()
enterprise.recibirAtaque(14)
enterprise.encontrarEscudo()
enterprise.potencia()
enterprise.coraza()