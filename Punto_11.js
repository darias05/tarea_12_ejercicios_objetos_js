// 11. Crear un metodo constructor llamado Libro. Sus atributos título del libro, autor, 
// número de ejemplares del libro y número de ejemplares prestados
// los siguiente metodos para la clase:

// préstamo() que incremente el atributo correspondiente cada vez que se realice un 
// préstamo del libro. No se podrán prestar libros de los que no queden ejemplares 
// disponibles para prestar. Devuelve true si se ha podido realizar la operación y false en caso contrario.
// devolución() que decremente el atributo correspondiente cuando se produzca la devolución de un libro. 
// No se podrán devolver libros que no se hayan prestado. Devuelve true si se ha podido realizar 
// la operación y false en caso contrario.
// toString() para mostrar los datos de los libros. Este método se heredada de Object y 
// lo debemos modificar (override) para adaptarlo a la clase Libro.

function molde_libro(titulo_ingresado, autor_ingresado, ejemplares_libro, libros_prestados){
    this.titulo = titulo_ingresado
    this.autor = autor_ingresado
    this.ejemplares = parseInt(ejemplares_libro)
    this.prestados = parseInt(libros_prestados)

    this.prestamo = function(valor_ingresado){
        if(this.ejemplares > 0){
            this.ejemplares--
            this.prestados++
            console.log(true)
        } else {
            console.log(false)
        }
    }

    this.devolucion = function(valor_suma){
        if(this.prestados > 0){
            this.ejemplares++
            this.prestados--
            console.log(true)
        } else {
            console.log(false)
        }
    }

    this.toString = function(){
        console.log(`Titulo = ${this.titulo}`)
        console.log(`Autor = ${this.autor}`)
        console.log(`Ejemplares = ${this.ejemplares}`)
        console.log(`Prestados = ${this.prestados}`)
    }
}

let libro_uno = new molde_libro("100 años de soledad", "Gabriel García Márquez", 100, 40)
libro_uno.prestamo(10)
libro_uno.devolucion(30)
libro_uno.toString()