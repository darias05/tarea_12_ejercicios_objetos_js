// 13. Agregar al metodo constructor de la Enterprise del ejercicio 12, la capacidad de entender estos
// mensajes.
// fortalezaDefensiva(), que es el máximo nivel de ataque que puede resistir,
// o sea, coraza más potencia.
// necesitaFortalecerse(), tiene que ser true si su coraza es 0 y su potencia
// es menos de 20.
// fortalezaOfensiva(), que corresponde a cuántos puntos de fuerza tendría
// un ataque de la Enterprise. Se calcula así: si tiene menos de 20 puntos de
// potencia entonces es 0, si no es (puntos de potencia - 20) / 2.

molde_enterprise.prototype.fortalezaDefensiva = function(){
    this.maximo_resistencia = this.potencia_enterprise + this.coraza_enterprise
    console.log(`Enterprise puede recibir un maximo de ataque de = ${this.maximo_resistencia}`)
}

molde_enterprise.prototype.necesitaFortalecerse = function(){
    if(this.coraza_enterprise == 0 && this.potencia_enterprise < 20){
        console.log(true)
    }
}

molde_enterprise.prototype.fortalezaOfensiva = function(){
    this.fuerza_ataque_enterprise = 0
    if(this.potencia_enterprise >= 20){
        this.fuerza_ataque_enterprise = (this.potencia_enterprise - 20) / 2
        console.log(`Fuerza de ataque de la Enterprise = ${this.fuerza_ataque_enterprise}`)
    }else{
        console.log(`Fuerza de ataque de la Enterprise = ${this.fuerza_ataque_enterprise}`)
    }
}

enterprise.fortalezaDefensiva()
enterprise.recibirAtaque(60)
enterprise.necesitaFortalecerse()
enterprise.encontrarPilaAtomica()
enterprise.fortalezaOfensiva()