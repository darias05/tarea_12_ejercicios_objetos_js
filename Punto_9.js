// 9. Agregar al metodo constructor de Chimuela del ejercicio 8, 
// la capacidad de entender estos mensajes:
// estaDebil(), Chimuela está débil si su energía es menos de 50.
// estaFeliz(), Chimuela está feliz si su energía está entre 500 y 1000.
// cuantoQuiereVolar(), que es el resultado de la siguiente cuenta. De base,
// quiere volar (energía / 5) kilómetros, p.ej., si tiene 120 de energía, quiere volar
// 24 kilómetros. Si la energía está entre 300 y 400, entonces hay que sumar 10
// a este valor, y si es múltiplo de 20, otros 15. Entonces, si Chimuela tiene 340 de
// energía, quiere volar 68 + 10 + 15 = 93 kilómetros. Para probar esto, sobre
// un REPL recién lanzado darle de comer 85 a Chimuela, así la energía queda en
// 340.
// Para saber si n es múltiplo de 20 hacer: n % 20 == 0. Probarlo en el 
// REPL(Read-Eval-Print-Loop)(Lectura-Evaluación-Impresión)

molde_chimuela.prototype.estaDebil = function(){
    if(this.cantidad_energia < 50){
        console.log(`Chimuela esta debil su energia es de ${this.cantidad_energia}`)
    }else{
        console.log(`Chimuela aun no esta debil, su energia es de ${this.cantidad_energia}`)
    }
}

molde_chimuela.prototype.estaFeliz = function(){
    if(this.cantidad_energia >= 500 && this.cantidad_energia <= 1000){
        console.log(`Chimuela esta feliz su energia es de ${this.cantidad_energia}`)
    }else{
        console.log(`Chimuela esta triste su energia es de ${this.cantidad_energia}`)
    }
}

molde_chimuela.prototype.cuantoQuiereVolar = function(){
    let quiere_volar = this.cantidad_energia / 5

    if(this.cantidad_energia >= 300 && this.cantidad_energia <= 400){
        quiere_volar = quiere_volar + 10
    } else if(this.cantidad_energia >= 300 && this.cantidad_energia <= 400){
        quiere_volar = quiere_volar + 10
    }

    if(this.cantidad_energia % 20 == 0){
        quiere_volar = quiere_volar +15
        console.log(`Chimuela quiere volar ${quiere_volar} Kilometros`)
    } else {
        console.log(`Chimuela quiere volar ${quiere_volar} Kilometros`)
    }
}

Chimuela.comer(85)
Chimuela.estaDebil()
Chimuela.estaFeliz()
Chimuela.cuantoQuiereVolar()