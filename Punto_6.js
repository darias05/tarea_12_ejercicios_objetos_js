//Implementar un objeto que modele un contador. Un contador se puede incrementar o decrementar, 
//recordando el valor actual. Al resetear un contador, se pone en cero.
//Además es posible indicar directamente cual es el valor actual. Este objeto debe entender los siguientes
// mensajes: 6.1 reset() 6.2 inc() 6.3 dec() 6.4 valorActual() 6.5 valorActual(nuevoValor) P.ej. si se 
//evalúa la siguiente secuencia contador.valorActual(10) contador.inc() contador.inc() contador.dec() 
//contador.inc() contador.valorActual() el resultado debe ser 12.

function contador(){
    this.valor=0;

    this.valor_Actual= function(cantidad){
        if(cantidad == null){
            console.log(`El valor actual es ${this.valor}`);
        }else{
            this.valor= parseInt(cantidad);
            this.type="valor actual"
        }
    }
    this.incrementar= function(){
        this.valor++
        this.type="Incremento actual"
    }

    this.decrementar= function(){
        this.valor--
        this.type="Decremento actual"
    }

    this.reset= function(){
        this.valor=0;
        this.type="Reset"
    }
}