//Crear un metodo constructor llamado contraseña. Sus atributos longitud y contraseña Construye los
// siguiente metodos para la clase:
//5.1 esFuerte(): devuelve un booleano si es fuerte o no, para que sea fuerte debe tener mas de 2 mayúsculas, 
//mas de 1 minúscula y mas de 5 números. 5.2 generarPassword(): genera la contraseña del objeto con la 
//longitud que tenga. 5.3 seguridadPaswword(); indicar si la contraseña es debil contiene entre 1 a 6
// caracteres (caracteres numeros y letras), media (7 a 10 caracteres numeros y letras) o fuerte 
//(11 a 20 caracteres letras y caracteres especiales)

function metodo_contrasena(contrasena, longitud){
    this.contrasena=contrasena;
    this.longitud=parseInt(longitud);
    this.esFuerte= function(){
        let mayusculas=0;
        let minuscula=0;
        let num=0;

        for(var i=0;i < this.longitud; i++){
            if(this.contrasena[i]==this.contrasena[i].toUpperCase() && this.contrasena[i] != parseInt(this.contrasena[i]) &&/A-Za-z0-9/.test(this.contrasena[i]) != false){
                mayusculas++
            }if(this.contrasena[i]==this.contrasena[i].toLowerCase() && this.contrasena[i] != parseInt(this.contrasena[i]) &&/A-Za-z0-9/.test(this.contrasena[i]) != false){
                minuscula++
            }if(this.contrasena[i]==parseInt(this.contrasena[i]) && /A-Za-z0-9/.test(this.contrasena[i]) != false){
                num++
            }
        }
        if (mayusculas >2 && minuscula >1 && num >5){
            return true;
        }else{
            return false;
        }
    }
    this.seguridadPaswword= function(){
        let letras=0;
        let num=0;
        let especiales=0;

        for(var i=0;i < this.longitud; i++){
            if(this.contrasena[i]==this.contrasena[i] && this.contrasena[i] != parseInt(this.contrasena[i]) &&/A-Za-z0-9/.test(this.contrasena[i]) != false){
                letras++
            }if(this.contrasena[i]== parseInt(this.contrasena[i]) &&/A-Za-z0-9/.test(this.contrasena[i]) != false){
                num++
            }if(/A-Za-z0-9/.test(this.contrasena[i]) == false){
                especiales++
            }
        }
        if(this.longitud <=6){
            console.log("contraseña debil")
        }else if(this.longitud >=7 && this.longitud <=10){
            console.log("contraseña media")
        }else{
            console.log("contraseña fuerte")
        }
    }
}