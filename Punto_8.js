// 8. Implementar un objeto que modele a Chimuela, una dragona de la que nos interesa
// saber qué energía tiene en cada momento, medida en joules. En el metodo constructor 
// simpli cado que nos piden implementar, las únicas acciones que vamos a contemplar
// son:
// cuando Chimuela come una cantidad de comida especi cada en gramos, en este
// caso adquiere 4 joules por cada gramo, y
// cuando Chimuela vuela una cantidad de kilómetros, en este caso gasta un joule
// por cada kilómetro, más 10 joules de  costo  jo  de despegue y aterrizaje.
// La energía de Chimuela nace en 0. El objeto que implementa este metodo constructor de Chimuela,
// debe entender los siguientes mensajes:
// 8.1 comer(gramos)
// 8.2 volar(kilometros)
// 8.3 energia()
// P.ej. si sobre un REPL(Read-Eval-Print-Loop)(Lectura-Evaluación-Impresión) 
// recién lanzado se evalúa la siguiente secuencia
// Chimuela.comer(100)
// Chimuela.volar(10)
// Chimuela.volar(20)
// Chimuela.energia()
// el resultado debe ser 350.

function molde_chimuela(){
    this.cantidad_energia = 0

    this.comer = function(comida_ingresada){
        this.cantidad_energia = 4 * comida_ingresada
    }

    this.volar = function(kilometros_ingresados){
        let despegue_aterrizaje = 10
        if(this.cantidad_energia == 0 || (this.cantidad_energia - parseInt(kilometros_ingresados) - despegue_aterrizaje) < 0){
            console.log(`Chimuela no puede volas mas ya que no tiene suficiente energia, por favor alimentala`)
        } else {
            this.cantidad_energia = this.cantidad_energia - parseInt(kilometros_ingresados) - despegue_aterrizaje
        }
    }

    this.energia = function(){
        console.log(`La energia actual de Chimuela es ${this.cantidad_energia}`)
    }
}

let Chimuela = new molde_chimuela()
Chimuela.comer(100)
Chimuela.volar(10)
Chimuela.volar(20)
Chimuela.energia()