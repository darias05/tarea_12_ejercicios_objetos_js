// 10. Implementar un objeto que represente una calculadora sencilla, que permita sumar,
// restar y multiplicar. Este objeto debe entender los siguientes mensajes:
// 10.1 cargar(numero)
// 10.2 sumar(numero)
// 10.3 restar(numero)
// 10.4 multiplicar(numero)
// 10.5 valorActual()
// P.ej. si se evalúa la siguiente secuencia
// calculadora.cargar(0)
// calculadora.sumar(4)
// calculadora.multiplicar(5)
// calculadora.restar(8)
// calculadora.multiplicar(2)
// calculadora.valorActual()
// el resultado debe ser 24.

function molde_calculadora(){
    this.numero = 0

    this.cargar = function(valor_ingresado){
        this.numero = valor_ingresado
    }

    this.sumar = function(valor_suma){
        this.numero = this.numero + valor_suma
    }

    this.restar = function(valor_resta){
        this.numero = this.numero - valor_resta
    }

    this.multiplicar = function(valor_multiplicacion){
        this.numero = this.numero * valor_multiplicacion
    }

    this.valorActual = function(){
        console.log(`El numero actual es ${this.numero}`)
    }
}

let calculadora = new molde_calculadora()
calculadora.cargar(0)
calculadora.sumar(4)
calculadora.multiplicar(5)
calculadora.restar(8)
calculadora.multiplicar(2)
calculadora.valorActual()