# Tarea_12_Ejercicios_Objetos_JavaScript
## Descripcion

En este proyecto se encuentran los 14 ejercicios de Objetos en JavaScript solucionados, cada punto tiene su codigo individual.

## Como clonar
> Para clonar el proyecto brindamos las siguientes 2 opciones.
 * **Clone with SSH:** 
 ```
 git@gitlab.com:darias05/tarea_12_ejercicios_objetos_js.git
 ```
 * **Clone with HTTPS:** 
 ```
 https://gitlab.com/darias05/tarea_12_ejercicios_objetos_js.git
 ```

## Autores
> Dylan Arias Arenas

> Ginna Paola Tangarife
