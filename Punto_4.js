//Crear un metodo constructor llamado persona. Sus atributos son: nombre, edad, DNI, 
//sexo (H hombre, M mujer), peso y altura Construye los siguiente metodos para la clase:
//4.1 calcularIMC(): calculara si la persona esta en su peso ideal (peso en kg/(altura^2 en m)),
// si esta fórmula devuelve un valor menor que 20, la función devuelve un -1, 
//si devuelve un número entre 20 y 25 (incluidos), significa que esta por debajo de su peso ideal 
//la función devuelve un 0 y si devuelve un valor mayor que 25 significa que tiene sobrepeso, 
//la función devuelve un 1. Te recomiendo que uses constantes para devolver estos valores. 
//4.2 esMayorDeEdad(): indica si es mayor de edad, devuelve un booleano. 4.3 comprobarSexo(char sexo): 
//comprueba que el sexo introducido es correcto. Si no es correcto, sera H.

function persona(nom_persona,edad_persona,DNI_persona, sexo_persona, peso_persona,altura_persona){
    this.nombre=nom_persona;
    this.edad=edad_persona;
    this.DNI=DNI_persona;
    this.sexo=sexo_persona;
    this.peso= peso_persona;
    this.altura=altura_persona;
    this.calcularIMC = function(){
        const menor_20= -1;
        const entre_20_25= 0;
        const sobrepeso= 1;

        this.operacion = this.peso /(this.altura^2)
        if (this.operacion < 20){
            return menor_20;
        }else if(this.operacion >= 20 && this.operacion <= 25){
            return entre_20_25;
        }else if(this.operacion > 25){
            return sobrepeso;
        }  
    }
    this.es_mayor_de_edad= function(){
        if(this.edad>=18){
            return true;
        }else{
            return false;
        }
    }
    this.comprobarSexo= function(){
        if(this.sexo=="M"){
            return `Es correcto, su sexo es: ${this.sexo}`;
        }else if(this.sexo=="H"){
            return `No es correcto, su sexo es: ${this.sexo}`;
        }
    }
}
    
